# Logic gates - neural network #
## AND ##
![and.png](https://bitbucket.org/repo/LyGkKa/images/1029654171-and.png)
## OR ##
![or.png](https://bitbucket.org/repo/LyGkKa/images/1532444366-or.png)
## NOT ##
![not.png](https://bitbucket.org/repo/LyGkKa/images/2379724201-not.png)
## NAND ##
![nand.png](https://bitbucket.org/repo/LyGkKa/images/3181633268-nand.png)
## XOR ##
![xor.png](https://bitbucket.org/repo/LyGkKa/images/3377749008-xor.png)